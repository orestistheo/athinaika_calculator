import sqlite3
from datetime import datetime
import pandas as pd
import configparser
import sqlalchemy
import os

# Get properties for database connectivity and mode from ConfigFile
thisfolder = os.path.dirname(os.path.abspath(__file__))
initfile = os.path.join(thisfolder, 'ConfigFile.properties')
config = configparser.RawConfigParser()
config.read(initfile)
database_username = config.get('DatabaseSection', 'database_username')
database_password = config.get('DatabaseSection', 'database_password')
database_ip = config.get('DatabaseSection', 'database_ip')
database_name = config.get('DatabaseSection', 'database_name')
data_importer_mode = config.get('DatabaseSection', 'database_mode')


def get_all_from_table():
    # create connection
    database_connection = sqlalchemy.create_engine('mysql+pymysql://{0}:{1}@{2}/{3}'.
                                                   format(database_username, database_password,
                                                          database_ip, database_name))
    metadata = sqlalchemy.MetaData()
    result_proxy = database_connection.execute('SELECT organizations.name, organizations.id FROM organizations')
    result_set = result_proxy.fetchall()
    return result_set


def execute_query(query_string):
    # create connection
    database_connection = sqlalchemy.create_engine('mysql+pymysql://{0}:{1}@{2}/{3}'.
                                                   format(database_username, database_password,
                                                          database_ip, database_name))
    metadata = sqlalchemy.MetaData()
    result_proxy = database_connection.execute(query_string)
    result_set = result_proxy.fetchall()
    return result_set


def insert_into_user_email(email, org_code, spec_code):
    # create connection
    database_connection = sqlalchemy.create_engine('mysql+pymysql://{0}:{1}@{2}/{3}'.
                                                   format(database_username, database_password,
                                                          database_ip, database_name))
    metadata = sqlalchemy.MetaData()
    database_connection.execute("""INSERT INTO user_email
                          (email,organization_code, speciality_code,submission_date) 
                           VALUES 
                          ('{email}','{org_code}', '{spec_code}','{subm_date}')""".format(email=email,
                                                                                          org_code=org_code,
                                                                                          spec_code=spec_code,
                                                                                          subm_date=datetime.today().strftime(
                                                                                              '%Y-%m-%d')))
    msg = "Εγγραφήκατε επιτυχώς στη λίστα"
    return msg


def execute_query_to_pandas(query_string):
    # create connection
    database_connection = sqlalchemy.create_engine('mysql+pymysql://{0}:{1}@{2}/{3}'.
                                                   format(database_username, database_password,
                                                          database_ip, database_name))
    result_proxy = database_connection.execute(query_string)
    result_set = result_proxy.fetchall()
    if len(result_set) == 0:
        return None
    else:
        result_dataframe = pd.DataFrame(result_set)
        result_dataframe.columns = result_set[0].keys()

    return result_dataframe
