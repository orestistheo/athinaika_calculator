import dash_bootstrap_components as dbc
import dash_html_components as html

header = html.Div([
    # Header component
    # Navbar
    dbc.Nav(className="nav nav-pills", children=[
        ## home_page
        dbc.NavItem(html.Div([
            dbc.NavLink("HOME", href="/", id="home-popover", active=False, style={'color': 'white'}),
            dbc.Popover(id="home", is_open=False, target="home-popover", children=[
                dbc.PopoverHeader("Home")
            ])
        ])),
        ## tracker
        dbc.NavItem(html.Div([
            dbc.NavLink("TRACKER", href="/tracker", id="tracker-popover", active=False, style={'color': 'white'}),
            dbc.Popover(id="tracker", is_open=False, target="tracker-popover", children=[
                dbc.PopoverHeader("Tracker")
            ])
        ])),
    ], style={"backgroundColor": "#6c757d", 'height': 40})
]
)

footer = html.Footer(
    id='footer-copyright',
    children=[
        html.Span(
            'Copyright © 2020 eidikothta',
            className='text-muted',
            style={'textAlign': 'center'}
        ),
        html.H5(""),
    ],
    className="row",
    style={
        'textAlign': 'center',
        'width': '100%',
        'padding': '10px 10px 0',
        # top right bottom left
        'marginLeft': 10,
        'marginTop': 20

    }
)
