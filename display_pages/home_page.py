from datetime import datetime

import dash as dash
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
import dash_table
import numpy as np
from dash.dependencies import Input, Output, State
from dateutil.relativedelta import relativedelta

import dbConn as dbConn
import shared_functions as func
import wait_time_prediction as predict
from app import app
from app_components import header, footer

no_info_string = 'Δεν υπάρχουν πληροφορίες για το συγκεκριμένο συνδυασμό'
ask_about_informing_on_mail_string = 'Θέλετε να ενημερώνεστε άμεσα στο e-mail σας για τις αλλαγές στη λίστα [{org_name} - {speciality_name}] ή και σε άλλες λίστες που μπορεί να σας ενδιαφέρουν ; Εγγραφείτε στην υπηρεσία Tracker  '
external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

select_specialty_query_string = "select specialities.name, specialities.id from top_strings left join specialities on top_strings.speciality_id = specialities.id WHERE top_strings.organization_id='{org_id}'"

wait_number_input = dbc.FormGroup(
    [
        dbc.Label("Αριθμός στη λίστα αναμονής *", html_for="wait_number", width=2),
        dbc.Col(
            dbc.Input(
                type="number", value=0, id="wait_number", placeholder="Εισάγετε τη σειρά αναμονής σας",
                style={'backgroundColor': 'white',
                       'border': 'groove', 'borderColor': '#ccc', 'borderRadius': 4, 'borderWidth': 1,
                       'borderSpacing': 0,
                       'height': 36}
            ),
            width=4,

        )
    ],
    row=True,
    style={'marginTop': 20, 'marginLeft': 20})

info_element = dbc.FormGroup([
    dbc.Label(html.P(" Αλλάζεις τους μήνες."), width=12),
    dbc.Label(html.P(" Αλλάζεις τις ημ/νίες λήξης σύμβασης."), width=12),
    dbc.Label(html.P(" Διαγράφεις άτομα ."), width=12),
    dbc.Label(html.P(" Το Calculator …υπολογίζει!"), width=12)
]
)

required_fields_label = dbc.Label("Υποχρεωτικά πεδία *", width=2)

# Initialization of dropdowns
items_list = dbConn.get_all_from_table()
organization_options = {}
organization_options_list = []
for item_component in items_list:
    organization_options = {'label': item_component[0], 'value': item_component[1]}
    organization_options_list.append(organization_options)

initial_organization_value = organization_options_list[0].get('value')
specialties_list = dbConn.execute_query(select_specialty_query_string.format(org_id=str(initial_organization_value)))
specialties_options = {}
specialties_options_list = []
for item_component in specialties_list:
    specialties_options = {'label': item_component[0], 'value': str(item_component[1])}
    specialties_options_list.append(specialties_options)

initial_speciality_value = specialties_options_list[0].get('value')

initial_dataframe = func.fetch_values_for_dataframe(initial_organization_value, initial_speciality_value)

organizations_dropdown = dbc.FormGroup(
    [
        dbc.Label("Επιλέξτε πρωτα νοσοκομείο για να ανανεωθούν αντίστοιχα οι ειδικότητες *",
                  html_for="organizations_drop_down", width=2),
        dbc.Col(
            dcc.Dropdown(
                id="organizations_drop_down",
                options=organization_options_list,
                value=initial_organization_value,
                optionHeight=35
            ),
            width=4,
        ),
    ],
    row=True,
    style={'marginTop': 20, 'marginLeft': 20}
)

speciality_dropdown = dbc.FormGroup(
    [
        dbc.Label("Ειδικότητα *", html_for="speciality_drop_down", width=2),
        dbc.Col(
            dcc.Dropdown(
                id="speciality_drop_down",
                options=specialties_options_list,
                value=initial_speciality_value,
                optionHeight=35
            ),
            width=4,
        ),
    ],
    row=True,
    style={'marginTop': 20, 'marginLeft': 20}
)

submit_button = html.Button("Submit", id='subm_btn', style={
    'height': 45, 'width': 90, 'marginTop': 20, 'marginLeft': '25%',
    'fontSize': 16, 'borderRadius': 6,
    'backgroundColor': '#6c757d', 'color': 'white'})

columns = ['Αριθμός στη λίστα', 'Ημερομηνία έναρξης', 'Ημερομηνία λήξης', 'Συνολική χρονική διάρκεια(μήνες)', 'Status']

data_table_component = dash_table.DataTable(
    id='data_table',
    style_table={'height': '200px', 'overflowY': 'auto', 'overflowX': 'auto', 'width': 1000},
    style_header={'whiteSpace': 'normal'},
    columns=[{
        'name': '{}'.format(i),
        'id': '{}'.format(i),
        'editable': func.get_editable_for_col(i),
        'type': func.get_type_for_col(i)
    } for i in columns],
    fixed_rows={'headers': True},
    style_cell={
        'width': 100,
        'textAlign': 'center'
    },
    editable=True,
    row_deletable=True
)

initial_datatable_component = data_table_component
initial_datatable_component.data = initial_dataframe.to_dict('records')

# App component containing all html elements and custom style
home_layout = html.Div([
    header,
    dbc.Form(
        [wait_number_input, organizations_dropdown, speciality_dropdown,
         required_fields_label, info_element, html.Div(initial_datatable_component, id='datatable_element'),
         submit_button])
    , html.Div(id='my-output')
    , footer
])


@app.callback(
    Output("datatable_element", "children"),
    [Input("organizations_drop_down", "value"), Input("speciality_drop_down", "value")])
def update_dataframe(organization_id, speciality_id):
    if organization_id is None or speciality_id is None:
        return None
    df_data = func.fetch_values_for_dataframe(organization_id, speciality_id)
    component_to_return = data_table_component
    if df_data is None:
        component_to_return.data = None
        return component_to_return
    component_to_return.data = df_data.to_dict('rows')
    return component_to_return


@app.callback(
    Output("speciality_drop_down", "options"),
    [Input("organizations_drop_down", "value")],
)
def update_specialties_on_org_selection(value):
    return func.get_specialities_for_organization(value=value)


@app.callback(
    Output('my-output', 'children'),
    [Input("organizations_drop_down", "value"), Input("speciality_drop_down", "value"), Input('subm_btn', 'n_clicks')],
    [State('datatable_element', 'children'), State('wait_number', 'value')])
def search_info_for_submission(organization_id, speciality_id, clicks, data_table_element, wait_number):
    if clicks is None:
        return None
    ctx = dash.callback_context
    component_id = ctx.triggered[0]['prop_id'].split('.')[0]
    if component_id != 'subm_btn':
        return
    else:
        if organization_id is None:
            return func.get_validation_div_with_msg('Επιλέξτε το νοσοκομείο')
        if speciality_id is None:
            return func.get_validation_div_with_msg('Επιλέξτε μια ειδικότητα')
        if not func.is_positive_int(wait_number):
            return func.get_validation_div_with_msg('Μη έγκυρος αριθμός αναμονής')
        data = data_table_element['props']['data']
        end_date_list = []
        wait_number_list = []
        time_person_wants_list = []
        status_list = []
        for i in range(len(data)):
            if not data[i]['Status'] == 'Σε ανάκληση':
                if data[i]['Ημερομηνία λήξης'] is None:
                    end_date_list.append(None)
                else:
                    end_date_list.append(datetime.strptime(data[i]['Ημερομηνία λήξης'], '%Y-%m-%d'))
            else:
                continue

        for i in range(len(data)):
            if not data[i]['Status'] == 'Σε ανάκληση':
                if data[i]['Αριθμός στη λίστα'] is None:
                    continue
                else:
                    wait_number_list.append(data[i]['Αριθμός στη λίστα'])
            else:
                continue

        statuses_appointed = ''
        statuses_by_appointment = ''
        priorities = ''
        period_n = ''
        statuses_recall = ''
        priority_read = ''
        org_name = ''
        speciality_name = ''
        task_id = ''
        slots = 0
        header_str = no_info_string
        summary_query_string = 'select max(summary.task_id) as task_id, slots , statuses_appointed , statuses_by_appointment, priorities, statuses_recall, priority_ready, period_n from summary WHERE summary.organization_id={org_id} and summary.speciality_id={spec_id} group by summary.task_id order by summary.task_id desc limit 1'.format(
            org_id=str(organization_id), spec_id=str(speciality_id))
        helper1 = dbConn.execute_query(summary_query_string)
        if helper1 is not None:
            slots = helper1[0][1]
            statuses_appointed = helper1[0][2]
            statuses_by_appointment = helper1[0][3]
            priorities = helper1[0][4]
            statuses_recall = helper1[0][5]
            priority_read = helper1[0][6]
            period_n = helper1[0][7]
            task_id = helper1[0][0]

        for i in range(len(data)):
            if data[i]['Status'] in ['Διορισμένος', 'Υπό διορισμό', 'Σε αναμονή', 'Σε ανάκληση', 'Άγνωστο status']:
                if data[i]['Status'] == 'Διορισμένος':
                    status_list.append(1)
                elif data[i]['Status'] == 'Υπό διορισμό':
                    status_list.append(2)
                elif data[i]['Status'] == 'Σε αναμονή':
                    status_list.append(3)
                elif data[i]['Status'] == 'Σε ανάκληση':
                    continue
                else:
                    status_list.append(0)
            if not data[i]['Status'] == 'Σε ανάκληση':
                if data[i]['Συνολική χρονική διάρκεια(μήνες)'] is None:
                    time_person_wants_list.append(int(float(period_n)))
                elif func.is_number(data[i]['Συνολική χρονική διάρκεια(μήνες)']) and int(
                        float(data[i]['Συνολική χρονική διάρκεια(μήνες)'])) >= 0:
                    time_person_wants_list.append(data[i]['Συνολική χρονική διάρκεια(μήνες)'])
                else:
                    return func.get_validation_div_with_msg('Μη έγκυρη χρονική διάρκεια στη γραμμή {i}'.format(i=i + 1))
            else:
                continue

        query_str = 'select top_strings.top_str, organizations.name, specialities.name from top_strings left join organizations on top_strings.organization_id=organizations.id left join specialities on top_strings.speciality_id=specialities.id  WHERE top_strings.organization_id = {org_id} and top_strings.speciality_id = {spec_id}'.format(
            org_id=str(organization_id), spec_id=str(speciality_id))
        recent_people_query_str = 'select submission_date, starting_date from recent_people where recent_people.task_id={task_id} ORDER by starting_date'.format(
            task_id=str(task_id))
        # query to fetch end date list of appointed
        helper = dbConn.execute_query(query_str)
        recent_people_helper = dbConn.execute_query(recent_people_query_str)
        if recent_people_helper is not None:
            first_subm_date = np.datetime64(recent_people_helper[0][0])
            second_subm_date = np.datetime64(recent_people_helper[1][0])
            third_subm_date = np.datetime64(recent_people_helper[2][0])
            first_start_date = np.datetime64(recent_people_helper[0][1])
            second_start_date = np.datetime64(recent_people_helper[1][1])
            third_start_date = np.datetime64(recent_people_helper[2][1])
            X_years, X_months, X_days = func.convert_days_to_years((first_start_date - first_subm_date).astype(int))
            Y_years, Y_months, Y_days = func.convert_days_to_years((second_start_date - second_subm_date).astype(int))
            Z_years, Z_months, Z_days = func.convert_days_to_years((third_start_date - third_subm_date).astype(int))
            average_years, average_months, average_days = func.convert_days_to_years((
                                                                                             (
                                                                                                     third_start_date - third_subm_date).astype(
                                                                                                 int) + (
                                                                                                     second_start_date -
                                                                                                     second_subm_date).astype(
                                                                                         int) + (
                                                                                                     first_start_date - first_subm_date).astype(
                                                                                         int)) / 3)
        if helper is not None:
            header_str = helper[0][0]
            org_name = helper[0][1]
            speciality_name = helper[0][2]
        deleted_before_wait_number = 0
        for i in range(wait_number):
            if i + 1 not in wait_number_list and i + 1 <= priorities:
                deleted_before_wait_number = deleted_before_wait_number + 1
            else:
                continue
        actual_wait_number = wait_number - deleted_before_wait_number
        if actual_wait_number <= 0:
            actual_wait_number = 1
        appointment_year, appointment_month, appointment_day = predict.predict_wait_time(slots, end_date_list,
                                                                                         time_person_wants_list,
                                                                                         status_list,
                                                                                         actual_wait_number,
                                                                                         period_n)
        starting_date_to = datetime.today() + relativedelta(years=appointment_year, months=appointment_month)
        starting_date_from = starting_date_to + relativedelta(months=-3)
        element = html.Div(
            [
                dbc.Row(
                    [
                        dbc.Col(html.Div(header_str), width=12, style={'fontSize': 20, 'fontWeight': 'bold'}),
                        dbc.Col(html.Div(
                            "Ο μέσος όρος των χρόνων αναμονής των 3 πιο πρόσφατων διορισθέντων είναι: Μ.Ο = {av_years} χρόνια , {av_months} μήνες και {av_days} μέρες".format(
                                av_years=average_years, av_months=average_months, av_days=average_days)), width=12
                            , style={'fontSize': 20, 'fontWeight': 'bold'}),
                    ], style={'marginTop': 50, 'marginLeft': 20}),

                dbc.Row(
                    [
                        dbc.Col(html.Div(
                            "Διορισμένοι : {statuses_appointed}".format(statuses_appointed=statuses_appointed)),
                            width=2, style={'fontSize': 16}),
                        dbc.Col(html.Div("Υπό Διορισμό : {statuses_by_appointment}".format(
                            statuses_by_appointment=statuses_by_appointment)), width=2, style={'fontSize': 16}),
                        dbc.Col(html.Div("Σε αναμονή : {priorities}".format(priorities=priorities)), width=2,
                                style={'fontSize': 16}),
                        dbc.Col(html.Div("Σε ανάκληση : {statuses_recall}".format(statuses_recall=statuses_recall)),
                                width=2,
                                style={'fontSize': 16}),
                    ], style={'marginTop': 50, 'marginLeft': 20}),

                dbc.Row(
                    [
                        dbc.Col(html.Div(html.P(
                            "Υπολογισμός αν ΟΛΟΙ όσοι έχεις αφήσει στη λίστα πάρουν τη θέση:"))),
                        dbc.Col(html.Div([html.P(
                            "Εάν πάνε όλοι όσοι είναι πιο πάνω στη λίστα θα χρειαστεί να περιμένετε: {years_wait} χρόνια και {months_wait} μήνες".format(
                                years_wait=appointment_year, months_wait=appointment_month),
                            style={"fontWeight": "bold"})
                            ,
                            html.P(
                                "Το προβλεπόμενο έτος – τρίμηνο έναρξης: {starting_date_from} χρόνια - {starting_date_to}".format(
                                    starting_date_to=starting_date_to.strftime('%m/%Y'),
                                    starting_date_from=starting_date_from.strftime('%m/%Y')),
                                style={"fontWeight": "bold"})
                        ]),
                            width=12, style={'fontSize': 16}),
                        dbc.Col(html.Div(html.P(
                            "Μπορείτε να δοκιμάσετε ξανά διαγράφοντας άτομα, ή αλλάζοντας τις ημερομηνίες λήξης σύμβασης ή και τη συνολική διάρκεια για κάποια άτομα για να δείτε πώς επηρεάζει την αναμονή")))
                    ], style={'marginTop': 50, 'marginLeft': 20}),
                dbc.Row(
                    [
                        dbc.Col(html.Div(
                            [html.P(
                                "Τελευταίος/-α διορίστηκε στις : {start3_date}".format(start3_date=third_start_date)),
                                html.P("Είχε κάνει αίτηση στις : {subm3_date}".format(subm3_date=third_subm_date)),
                                html.P("Περίμενε {years3} χρόνια , {months3} μήνες και {days3} μέρες".format(
                                    years3=Z_years, months3=Z_months, days3=Z_days))]), width=3,
                            style={'fontSize': 16}),
                        dbc.Col(html.Div(
                            [html.P(
                                "Προηγουμένως κάποιος/-α διορίστηκε στις : {start3_date}".format(
                                    start3_date=second_start_date)),
                                html.P("Είχε κάνει αίτηση στις : {subm2_date}".format(subm2_date=second_subm_date)),
                                html.P("Περίμενε {years2} χρόνια , {months2} μήνες και {days2} μέρες".format(
                                    years2=Y_years, months2=Y_months, days2=Y_days))]), width=3,
                            style={'fontSize': 16}),
                        dbc.Col(html.Div(
                            [html.P(
                                "Προηγουμένως κάποιος/-α διορίστηκε στις : {start1_date}".format(
                                    start1_date=first_start_date)),
                                html.P("Είχε κάνει αίτηση στις : {subm1_date}".format(subm1_date=first_subm_date)),
                                html.P("Περίμενε {years1} χρόνια , {months1} μήνες και {days1} μέρες".format(
                                    years1=X_years, months1=X_months, days1=X_days))]), width=3, style={'fontSize': 16})
                    ], style={'marginTop': 50, 'marginLeft': 20}
                ),
                dbc.Row(
                    [
                        dbc.Col(html.Div([
                            html.Span(ask_about_informing_on_mail_string.format(org_name=org_name,
                                                                                speciality_name=speciality_name)),
                            dcc.Link('εδώ', href='/tracker', style={'color': 'blue'})]),
                            width=11, style={'fontSize': 20, 'fontWeight': 'bold'}),
                    ], style={'marginTop': 50, 'marginLeft': 20}),
                dbc.Row(
                    [
                        dbc.Col(html.Div([
                            html.Span("Για περισσότερες πληροφορίες για την υπηρεσία Tracker πάτα "),
                            html.A('εδώ', href='http://potempaino.gr/tracker.html', target="_blank",
                                   style={'color': 'blue'})]),
                            width=11, style={'fontSize': 20, 'fontWeight': 'bold'}),
                    ], style={'marginTop': 20, 'marginLeft': 20})
            ]
        )
        return element


@app.callback(
    [Output('data_table', 'data'), Output('subm_btn', 'disabled')],
    [Input('data_table', 'data_timestamp')],
    [State('data_table', 'data')])
def update_columns(timestamp, rows):
    if rows is None:
        return None, False
    is_disabled = False
    for row in rows:
        if row['Ημερομηνία λήξης'] is not None and row[
            'Ημερομηνία έναρξης'] is not None:
            if row['Συνολική χρονική διάρκεια(μήνες)'] is None:
                months = 0
            elif row['Συνολική χρονική διάρκεια(μήνες)'] is not None and int(
                    float(row['Συνολική χρονική διάρκεια(μήνες)'])) < 0:
                is_disabled = True
                months = 0
            else:
                months = int(
                    float(row['Συνολική χρονική διάρκεια(μήνες)']))
            row['Ημερομηνία λήξης'] = datetime.strptime(row['Ημερομηνία έναρξης'], '%Y-%m-%d').date() + relativedelta(
                months=months)
    return rows, is_disabled
