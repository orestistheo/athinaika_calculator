import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
import dash as dash
from app import app
from dash.dependencies import Input, Output
from app_components import header, footer
import dbConn as dbConn
import shared_functions as func

tracker_intro_text = "Καλωσήρθατε στην υπηρεσία Tracker. "
select_specialty_query_string = "select specialities.name, specialities.id from top_strings left join specialities on top_strings.speciality_id = specialities.id WHERE top_strings.organization_id='{org_id}'"

items_list = dbConn.get_all_from_table()
organization_options = {}
organization_options_list = []
for item_component in items_list:
    organization_options = {'label': item_component[0], 'value': item_component[1]}
    organization_options_list.append(organization_options)

initial_organization_value = organization_options_list[0].get('value')
specialties_list = dbConn.execute_query(select_specialty_query_string.format(org_id = str(initial_organization_value)))
specialties_options = {}
specialties_options_list = []
for item_component in specialties_list:
    specialties_options = {'label': item_component[0], 'value': str(item_component[1])}
    specialties_options_list.append(specialties_options)

initial_speciality_value = specialties_options_list[0].get('value')

email_input = dbc.FormGroup(
    [
        dbc.Label("Email", html_for="email", width=2),
        dbc.Col(
            dbc.Input(
                type="email", minLength= 6, id="email", placeholder="Εισάγετε το email σας",
                style={'backgroundColor': 'white', 'marginTop': 20,
                       'border': 'groove', 'borderColor': '#ccc', 'borderRadius': 4, 'borderWidth': 1,
                       'borderSpacing': 0,
                       'height': 36}
            ),
            width=4,

        ),
    ],
    row=True,
    style={'marginTop': 20, 'marginLeft': 20})

submit_email_button = html.Button("Submit", id='subm_btn_email', style={
    'height': 45, 'width': 90, 'marginTop': 20, 'marginLeft': '25%',
    'fontSize': 16, 'borderRadius': 6,
    'backgroundColor': '#6c757d', 'color': 'white'})

organizations_dropdown = dbc.FormGroup(
    [
        dbc.Label("Νοσοκομείο", html_for="tracker_organizations_drop_down", width=2),
        dbc.Col(
            dcc.Dropdown(
                id="tracker_organizations_drop_down",
                options=organization_options_list,
                value=initial_organization_value,
                optionHeight=35
            ),
            width=4,
        ),
    ],
    row=True,
    style={'marginTop': 20, 'marginLeft': 20}
)

speciality_dropdown = dbc.FormGroup(
    [
        dbc.Label("Ειδικότητα", html_for="tracker_speciality_dropdown", width=2),
        dbc.Col(
            dcc.Dropdown(
                id="tracker_speciality_dropdown",
                options=specialties_options_list,
                value=initial_speciality_value,
                optionHeight=35
            ),
            width=4,
        ),
    ],
    row=True,
    style={'marginTop': 20, 'marginLeft': 20}
)

# App component containing all html elements and custom style
tracker_layout = html.Div([
    header,
    dbc.Form([email_input, organizations_dropdown, speciality_dropdown, submit_email_button])
    , html.Div(id='my-output1')
    , footer
])


@app.callback(
    Output("tracker_speciality_dropdown", "options"),
    [Input("tracker_organizations_drop_down", "value")],
)
def update_specialties_on_org_selection(value):
    return func.get_specialities_for_organization(value=value)


@app.callback(
    Output("my-output1", "children"),
    [Input("email", "value"), Input("tracker_organizations_drop_down", "value"), Input("tracker_speciality_dropdown", "value"),
     Input('subm_btn_email', 'n_clicks')])
def subscribe_with_email_for_task(email, org_id, spec_id, clicks):
    if clicks is None:
        return None
    ctx = dash.callback_context
    component_id = ctx.triggered[0]['prop_id'].split('.')[0]
    if component_id != 'subm_btn_email':
        return
    else:
        if email is None:
            return func.get_validation_div_with_msg("Συμπληρώστε το email σας")
        if spec_id is None:
            return func.get_validation_div_with_msg("Πρέπει να επιλέξετε ειδικότητα για να εγγραφείτε")
        task_id = dbConn.execute_query(
            'select max(summary.task_id), organizations.code, specialities.code as task_id from summary LEFT JOIN organizations on organizations.id = summary.organization_id left join specialities on specialities.id = summary.speciality_id WHERE summary.organization_id={org_id} and summary.speciality_id={spec_id}'.format(
                org_id=str(org_id), spec_id=str(spec_id)))
        result = validate_input(email, task_id[0][1], task_id[0][2])
        if result[0]:
            msg = dbConn.insert_into_user_email(email, task_id[0][1], task_id[0][2])
            return func.get_validation_div_with_msg(msg)
        else:
            return func.get_validation_div_with_msg(result[1])


def validate_input(email, organization_code, speciality_code):
    # validate email
    is_valid = func.check_email(email)
    if not is_valid:
        return False, 'Λάθος email'
    # validate existing subscription for task
    result = dbConn.execute_query(
        "select count(user_email.id) from user_email where user_email.email = '{email}' and user_email.organization_code = '{organization_code}' and user_email.speciality_code = '{speciality_code}'".format(
            email=email, organization_code=organization_code,speciality_code =speciality_code))
    if result is None:
        return 'Internal Server Error'
    if result[0][0] >= 1:
        return False, 'Έχετε ήδη εγγραφεί στη συγκεκριμένη λίστα'
    else:
        return True, ''
