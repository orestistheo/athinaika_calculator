from datetime import datetime
from dateutil.relativedelta import relativedelta
import shared_functions as func
import dbConn as dbConn
from operator import itemgetter


def do_prediction(new_end_date_list, time_person_wants_list, wait_number, slots, aver_time_for_task):
    if slots > len(new_end_date_list):
        helper_list = new_end_date_list
    else:
        helper_list = new_end_date_list[0:slots]
    starting_point = 0
    list_number = 0
    while True:
        found, wait_time, list_number = check_for_number(helper_list, wait_number, list_number)
        if found:
            return wait_time
        else:
            starting_point = starting_point + slots
            helper_list = sorted(helper_list, key=itemgetter(1))
            helper_list = re_calculate_end_dates(new_end_date_list, helper_list, starting_point, slots,
                                                 time_person_wants_list,
                                                 aver_time_for_task)


def check_for_number(helper_list, wait_number, list_number):
    for i in range(len(helper_list)):
        if helper_list[i][0] == 3:
            list_number = list_number + 1
            if list_number == wait_number:
                wait_time = (helper_list[i][1] - datetime.today().date()).days - helper_list[i][2] * 30
                if wait_time < 0:
                    return True, 90, None
                else:
                    return True, abs(wait_time), None
    return False, None, list_number


def re_calculate_end_dates(new_end_date_list, helper_list_old, starting_point, slots, time_person_wants_list,
                           average_time_for_task_id):
    helperList = []
    for i in range(starting_point, slots + starting_point):
        if i >= len(new_end_date_list):
            helperList.append((3, (helper_list_old[i - starting_point][1] + relativedelta(
                months=3 + int(float(average_time_for_task_id)))), int(float(average_time_for_task_id))))
        elif new_end_date_list[i][0] == 1:
            helperList.append(
                (1, (helper_list_old[i - starting_point][1] + relativedelta(
                    months=(3 + int(float(time_person_wants_list[i]))))), int(float(time_person_wants_list[i]))))
        elif new_end_date_list[i][0] == 2:
            helperList.append(
                (2, (helper_list_old[i - starting_point][1] + relativedelta(
                    months=(3 + int(float(time_person_wants_list[i]))))), int(float(time_person_wants_list[i]))))
        elif new_end_date_list[i][0] == 3:
            helperList.append(
                (3, (helper_list_old[i - starting_point][1] + relativedelta(
                    months=3 + int(float(time_person_wants_list[i])))), int(float(time_person_wants_list[i]))))
        # for people with status 4 (σε ανάκληση) we have to figure out whether we have to process them
        elif new_end_date_list[i][0] == 4:
            helperList.append(
                (4, None))
        else:
            helperList.append((4, None))
    return helperList


def prepare_data(end_date_list, status_list, time_person_wants_list, slots, average_time_for_task_id):
    new_end_date_list = []
    list_len = len(end_date_list)
    if list_len >= slots:
        max = list_len
    else:
        max = slots
    for i in range(max):
        if i >= len(end_date_list):
            new_end_date_list.append((3, (
                    datetime.today() + relativedelta(months=3 + int(float(average_time_for_task_id)))).date(),
                                      int(float(average_time_for_task_id))))
        elif end_date_list[i] is None:
            if status_list[i] == 2:
                new_end_date_list.append(
                    (2, (datetime.today() + relativedelta(months=3 + int(float(time_person_wants_list[i])))).date(),
                     int(float(time_person_wants_list[i]))))
            elif status_list[i] == 3:
                end_date = calculate_end_date(slots, i, new_end_date_list, average_time_for_task_id)
                new_end_date_list.append(
                    (3, (end_date + relativedelta(months=3 + int(float(time_person_wants_list[i])))),
                     int(float(time_person_wants_list[i]))))

            # for people with status 4 (σε ανάκληση) we have to figure out whether we have to process them
            elif status_list[i] == 4:
                new_end_date_list.append(
                    (4, None, None))
        else:
            new_end_date_list.append((1, end_date_list[i].date(), int(float(time_person_wants_list[i]))))
    new_end_date_list = sorted(new_end_date_list, key=itemgetter(1))
    return new_end_date_list


def predict_wait_time(slots, end_date_list, time_person_wants_list, status_list, wait_number, aver_time_for_task):
    new_end_date_list = prepare_data(end_date_list, status_list, time_person_wants_list, slots, aver_time_for_task)
    return func.convert_days_to_years(
        do_prediction(new_end_date_list, time_person_wants_list, wait_number, slots, aver_time_for_task))


def search_info_for_task(organization_id, speciality_id, clicks):
    if clicks is None:
        return None
    # ctx = dash.callback_context
    # component_id = ctx.triggered[0]['prop_id'].split('.')[0]
    # if component_id == 'subm_btn':
    #     return None
    else:
        if organization_id is None:
            return None
        if speciality_id is None:
            return None
        result = dbConn.execute_query(
            'select max(summary.task_id) as task_id, summary.statuses_by_appointment, summary.statuses_appointed from summary WHERE summary.organization_id={org_id} and summary.speciality_id={spec_id}'.format(
                org_id=str(organization_id), spec_id=str(speciality_id)))
        task_id = result[0][0]
        statuses_appointed = result[0][2]
        #         do query to fetch end date list of appointed
        fetch_end_dates_query_string = "select starting_date, time_person_wants from persons where persons.task_id ={task_id} and persons.status =1".format(
            task_id=task_id)
        helper = dbConn.execute_query(fetch_end_dates_query_string)
        end_date_list = []
        for i in range(len(helper)):
            time_person_wants = helper[i][1]
            if time_person_wants == 'r':
                # figure out value of r
                months = 40
            else:
                months = int(helper[i][1])
            end_date_list.append(datetime.strptime(helper[i][0], '%Y-%m-%d') + relativedelta(months=months))


def calculate_end_date(slots, index, end_date_list, average_time_for_task_id):
    if index < slots:
        return datetime.today().date()
    removed_elements = index % slots
    calc_index = index / slots
    helper_list = end_date_list[(int((calc_index - 1)) * slots):(index - removed_elements)]
    helper_list = sorted(helper_list, key=itemgetter(1))
    return helper_list[removed_elements][1]
