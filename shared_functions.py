import calendar
from datetime import datetime
import random as rnd
import dash_bootstrap_components as dbc
import dash_html_components as html
import pandas as pd
from dateutil.relativedelta import relativedelta
import re
import dbConn as dbConn

select_specialty_query_string = 'select specialities.name, specialities.id from top_strings left join specialities on top_strings.speciality_id = specialities.id WHERE top_strings.organization_id='


def get_specialities_for_organization(value):
    spec_options_list = []
    if value is None:
        return spec_options_list
    else:
        spec = dbConn.execute_query(select_specialty_query_string + str(value) + ';')

        for item_comp in spec:
            spec_options = {'label': item_comp[0], 'value': item_comp[1]}
            spec_options_list.append(spec_options)
        return spec_options_list


def get_validation_div_with_msg(msg):
    return html.Div(
        [dbc.Row(
            [
                dbc.Col(html.Div(msg), width=5, style={'fontSize': 20, 'fontWeight': 'bold'}),
            ], style={'marginTop': 20, 'marginLeft': 20})])


# Python3 code to convert given
# number of days in terms of
# Years, Weeks and Days

# Function to find
# year, week, days
def convert_days_to_years(number_of_days):
    DAYS_IN_MONTH = 30
    # Assume that years is
    # of 365 days
    year = int(number_of_days / 365)
    month = int((number_of_days % 365) /
                DAYS_IN_MONTH)
    days = (number_of_days % 365) % DAYS_IN_MONTH
    return year, month, round(days)


def fetch_values_for_dataframe(organization_id, speciality_id):
    if organization_id is None:
        return pd.DataFrame()
    if speciality_id is None:
        return pd.DataFrame()
    result = dbConn.execute_query(
        "select max(summary.task_id) as task_id, summary.period_n from summary WHERE summary.organization_id='{org_id}' and summary.speciality_id={spec_id} group by summary.task_id order by summary.task_id desc limit 1".format(
            org_id=str(organization_id), spec_id=str(speciality_id)))
    if len(result) == 0:
        return None
    task_id = result[0][0]
    max_period = result[0][1]
    if task_id is None:
        return None
    fetch_end_dates_query_string = 'select starting_date, time_person_wants, status, id from persons where persons.task_id = {task_id} and status IN (1,2,3,4)'.format(
        task_id=task_id)
    data_frame = dbConn.execute_query_to_pandas(fetch_end_dates_query_string)
    if data_frame is None:
        return None
    start_date_list = data_frame['starting_date']
    time_person_wants_list = data_frame['time_person_wants']
    status_list = data_frame['status']
    start_date_series = []
    end_date_series = []
    status_series = []
    id_series = []
    wait_no = 0
    for i in range(len(status_list)):
        x = status_list[i]

        if x == 1:
            status_series.append('Διορισμένος')
            start_date_series.append(start_date_list[i])
            id_series.append(None)
        elif x == 2:
            status_series.append('Υπό διορισμό')
            start_date_series.append(datetime.today().date())
            id_series.append(None)
        elif x == 3:
            status_series.append('Σε αναμονή')
            start_date_series.append(None)
            wait_no = wait_no + 1
            id_series.append(wait_no)
        elif x == 4:
            status_series.append('Σε ανάκληση')
            start_date_series.append(start_date_list[i])
            wait_no = wait_no + 1
            id_series.append(wait_no)
        else:
            status_series.append('Άγνωστο status')
            start_date_series.append(None)
    for i in range(len(start_date_series)):
        x = time_person_wants_list[i]
        if x == 'r':
            if max_period > 12:
                x = rnd.randint(12, max_period - 12)
            else:
                x = 12
            time_person_wants_list[i] = x
        start_date = start_date_series[i]
        try:
            if start_date is None:
                end_date_series.append(None)
            else:
                float_x = float(x)
                end_date_series.append((start_date_series[i] + relativedelta(months=int(float_x))))
        except calendar.IllegalMonthError as e:
            print(e)

    data_frame['Ημερομηνία λήξης'] = end_date_series
    data_frame['Status'] = status_series
    data_frame['Αριθμός στη λίστα'] = id_series
    data_frame['Ημερομηνία έναρξης'] = start_date_series
    data_frame['Συνολική χρονική διάρκεια(μήνες)'] = time_person_wants_list
    data_frame = data_frame[
        ['Αριθμός στη λίστα', 'Ημερομηνία έναρξης', 'Ημερομηνία λήξης', 'Συνολική χρονική διάρκεια(μήνες)', 'Status']]
    return data_frame


def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False


def is_positive_int(s):
    try:
        x = int(s)
        if x > 0:
            return True
        else:
            return False
    except ValueError:
        return False


def get_editable_for_col(col_name):
    if col_name in ['Συνολική χρονική διάρκεια(μήνες)']:
        return True
    else:
        return False


def get_type_for_col(col_name):
    if col_name in ['Ημερομηνία έναρξης', 'Ημερομηνία λήξης']:
        return 'datetime'
    elif col_name == 'Status':
        return 'text'
    elif col_name in ['Αριθμός στη λίστα', 'Συνολική χρονική διάρκεια(μήνες)']:
        return 'numeric'
    else:
        return None


regex = '^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$'


def check_email(email):
    # pass the regular expression
    # and the string in search() method
    if re.search(regex, email):
        return True

    else:
        return False
