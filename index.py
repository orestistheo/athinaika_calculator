import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
from app import app
from display_pages.home_page import home_layout
from display_pages.tracker_page import tracker_layout

app.layout = html.Div([
    dcc.Location(id='url', refresh=False),
    html.Div(id='page-content')
])


@app.callback(Output('page-content', 'children'),
              [Input('url', 'pathname')])
def display_page(pathname):
    if pathname == '/':
        layout = home_layout
    elif pathname == '/tracker':
        layout = tracker_layout
    else:
        layout = '404'
    return layout


server = app.server

if __name__ == '__main__':
    app.config['suppress_callback_exceptions'] = True
    app.run_server(debug=True)
